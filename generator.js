
var bella = require('bellajs');

var generate = () => {

  let checked = bella.random(0, 1);

  let serialId = bella.leftPad(bella.random(0, 999), 3);
  let serial = `DVT${serialId}`;

  let begin = bella.leftPad(bella.random(0, 9999), 4);
  let end = bella.leftPad(bella.random(0, 99999), 5);
  let imei = `${begin}960079${end}`;

  let simTypes = bella.stabilize(['BT', 'ETH']);
  let simType = simTypes.pick();

  let iccid = [
    bella.rightPad(bella.random(0, 99999), 5),
    bella.leftPad(bella.random(0, 99999), 5),
    bella.leftPad(bella.random(0, 99999), 5),
    bella.leftPad(bella.random(0, 99999), 5)
  ].map((n) => {
    return String(n);
  }).reduce((prev = '', curr) => {
    return prev + curr;
  });

  let batches = bella.stabilize(['DVT1', 'DVT2', 'MP1', 'EVT1', 'EVT2', 'Demo', 'BB02', 'BB02', 'PVT']);
  let batch = batches.pick();

  let firmwares = bella.stabilize(['MP', 'R3', 'MP', 'R3', 'MPDemo']);
  let fm = firmwares.pick();

  let locations = bella.stabilize(['GJack', 'Ethiopia', 'In Transit to Ethiopia', 'NY', 'SF', 'Shipped', 'Not shipped']);
  let location = locations.pick();

  let products = bella.stabilize(['Outpour 1', 'Outpour 2', 'Cascade']);
  let prod = products.pick();

  let t = bella.now() - bella.random(1000 * 60 * 5, 1000 * 60 * 60 * 24 * 7);
  let time = bella.date.relativize(new Date(t));

  return {
    checked,
    serial,
    imei,
    prod,
    simType,
    iccid,
    batch,
    fm,
    location,
    time
  };
};

var make = (total) => {
  let a = [];
  let max = total * 10;
  for (let i = 0; i < max; i++) {
    a.push(generate());
  }
  let b = bella.stabilize(a);
  return b.pick(total);
};


var buildData = (size = 30) => {

  let b = make(size);
  let c = [];

  let d = [];
  let e = [];

  b.forEach((item) => {

    let {
      checked,
      serial,
      imei,
      prod,
      simType,
      iccid,
      batch,
      fm,
      location,
      time,
    } = item;

    let tick = checked === 1 ? 'x' : '';

    let s = `[${tick}], ${serial}, ${imei}, ${prod}, ${simType}, ${iccid}, ${batch}, ${fm}, ${location}, ${time}`;
    d.push(s);
  });

  b.forEach((item) => {

    let {
      checked,
      serial,
      imei,
      prod,
      simType,
      iccid,
      batch,
      fm,
      location,
      time,
    } = item;

    if (checked) {
      c.push(item);
    }
  });


  c.forEach((item) => {

    let {
      checked,
      serial,
      imei,
      prod,
      simType,
      iccid,
      batch,
      fm,
      location,
      time,
    } = item;

    let tick = checked === 1 ? 'x' : '';

    let s = `[${tick}], ${serial}, ${imei}, ${prod}, ${simType}, ${iccid}, ${batch}, ${fm}, ${location}, ${time}`;
    e.push(s);
  });

  return {
    mixed: b,
    checked: c,
    mixedForAlouka: d,
    checkedForAlouka: e
  }
};

var writeConsole = () => {

  let data = buildData();

  let b = data.mixed;
  let c = [];

  console.log('\n Original, no item checked:');
  b.forEach((item) => {

    let {
      checked,
      serial,
      imei,
      prod,
      simType,
      iccid,
      batch,
      fm,
      location,
      time,
    } = item;


    let s = `[], ${serial}, ${imei}, ${prod}, ${simType}, ${iccid}, ${batch}, ${fm}, ${location}, ${time}`;
    console.log(s);

  });

  console.log('\n The same list, checked some:');
  b.forEach((item) => {

    let {
      checked,
      serial,
      imei,
      prod,
      simType,
      iccid,
      batch,
      fm,
      location,
      time,
    } = item;

    let tick = checked === 1 ? 'x' : '';

    let s = `[${tick}], ${serial}, ${imei}, ${prod}, ${simType}, ${iccid}, ${batch}, ${fm}, ${location}, ${time}`;
    console.log(s);

    if (checked) {
      c.push(item);
    }
  });


  console.log('\n Checked items only:');
  c.forEach((item) => {

    let {
      checked,
      serial,
      imei,
      prod,
      simType,
      iccid,
      batch,
      fm,
      location,
      time,
    } = item;

    let tick = checked === 1 ? 'x' : '';

    let s = `[${tick}], ${serial}, ${imei}, ${prod}, ${simType}, ${iccid}, ${batch}, ${fm}, ${location}, ${time}`;
    console.log(s);
  });

};

writeConsole();

module.exports = {
  buildData
};
