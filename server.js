var koa = require('koa');
var json = require('koa-json');

var sensor = require('./generator');

var app = koa();

app.use(json());

app.use(function *(next){
  this.body = sensor.buildData();
});

const port = 3000;

app.listen(port, () => {
  console.log('\n Server started');
  console.log(`http://127.0.0.1:${port}`);
});
